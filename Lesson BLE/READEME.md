## Parts from AdaFruit 
[Adafruit Feather nRF52840 Express](https://www.adafruit.com/product/4062)  
[Adafruit I2S MEMS Microphone Breakout](https://www.adafruit.com/product/3421)  
[Adafruit LSM6DS33 + LIS3MDL - 9 DoF IMU](https://www.adafruit.com/product/4485)  
[STEMMA JST 4-pin to Cable](https://www.adafruit.com/product/4209)  

## Lessons from AdaFruit
[Intro to Feather BLE](https://learn.adafruit.com/introducing-the-adafruit-nrf52840-feather)  
[Intro to MEMS Microphone](https://learn.adafruit.com/adafruit-i2s-mems-microphone-breakout)  

[Accelerometer and Gyro](https://learn.adafruit.com/lsm6ds33-6-dof-imu=accelerometer-gyro)  
[Magnetometer](https://learn.adafruit.com/lis3mdl-triple-axis-magnetometer)  
