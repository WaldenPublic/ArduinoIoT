**Digital Input, Output**  
https://learn.adafruit.com/adafruit-io-basics-digital-input/adafruit-io-setup  
https://learn.adafruit.com/adafruit-io-basics-digital-output/adafruit-io-setup-1  

**Analog Input, Output**  
https://learn.adafruit.com/adafruit-io-basics-analog-input  
https://learn.adafruit.com/adafruit-io-basics-analog-output  

**Color**  
https://learn.adafruit.com/adafruit-io-basics-color  

**Temperature and Humidity**  
https://learn.adafruit.com/adafruit-io-basics-temperature-and-humidity 

**Servos**  
https://learn.adafruit.com/adafruit-io-basics-servo 

**Arduino IDE**  
https://www.arduino.cc/en/Main/Software  

**Arduino Drivers**  
https://github.com/adafruit/Adafruit_Windows_Drivers/releases  
https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers  

**Boards and Libraries**  
https://adafruit.github.io/arduino-board-index/package_adafruit_index.json  
http://arduino.esp8266.com/stable/package_esp8266com_index.json  

SSID:  creatorscorner  
Password:  adafruit





*Copyright (c) 2018 by Walden Geographic*
